# SET-TOP BOX STAKER

## Also, if you are compiling on anything with < 2GB of ram then increase your swapfle size to >= 2GB

## Overview

The is an attempt to to setup an inexpensive android settop box as a crypto staking machine.
Currently this proceedure as only been tested using a tinkerboard on a scratch Armbian Bionic 18.04 LTS build. This document is an excerpt of the full Divi Armbian document to build a set top box masternode from scratch. Once the set top build is ready and sanity checked, the document will be provided to Divi. Much thanks to the Divi dev team for helping me regain my focus on coding. https://diviproject.org.

## Official Armbian image for Pine A64 and one I created that includes all of the divi-cli dependencies

Official - Armbian Repository https://www.armbian.com/

W.I.P. Added context for what we are setting up to do.

## Get bitcoin db repo

apt-add-repository ppa:bitcoin/bitcoin

## Update your packages

```shell
$ sudo apt update
$ sudo apt upgrade -y
```

## Packages needed for the build

sudo apt install git-core libssl-dev libboost-all-dev libdb4o-cil-dev libevent-dev libzmq3-dev autoconf tmux vim automake libtool libdb4.8-dev libdb4.8++-dev software-properties-common -y

## Getting Divi

```shell
$ git clone https://github.com/divicoin/divi.git
$ cd divi/divi
```

## Configure and Build Divi

1. Initial test builds did not include the gui. GUI builds are currently in work.
2. The Divi source was potentially baselined against Armbian 16.04 LTS and has openssl 1.0 and libdb 4.8 dependency warnings

```shell
$ ./autogen.sh
$ ./configure --disable-tests --without-gui --with-unsupported-ssl
$ make
$ make install
```

## Preparing To Launch Divid

In order to execute the divid server you must create a configuration file containing an rpc user and rpc password. The default location for the divi configuration file is /home/(user)/.divi/divi.conf

Create a file configuration file containing the following: in the /home/(username)/.divi/divi.config

```config
##
## divi.conf configuration file. Lines beginning with # are comments.
##

rpcuser=<rpcusername>
rpcpassword=<rpcpassword>
rpcport=51473
rpcallowip=127.0.0.1
rpcconnect=127.0.0.1
reindex=1
daemon=1
```

## Starting divid

### Once the config file is created the launch divid as follows

```shell
$ divid
```
You should recieve a resonse that divi has started and can communicate vi the divi-cli client

**Type the following commands to verify the status of the new node**

```shell
$ divi-cli mnsync status            // Returns the synchronization status of your nodes
$ divi-cli addnode **ipaddr** adding  // Adds additional communication nodes on the network
```

**Obtain a node list of from the link below**
[Divi Network Nodes](https://chainz.cryptoid.info/divi/#!network)

## TODO

- Compiling with 2GBs of RAM is slow. Working on adding custom scripts to build Divi into the Armbian image
- Attempt build with unit tests for verification
- Add script to add the additonal nodes for syncing after initial setup
- Create a boilerplate divi.conf file this might be something for the divi team.
- Setup a friggin github repo! Here it is!!!!

## Deb package build notes thanks to this site

https://linuxconfig.org/easy-way-to-create-a-debian-package-and-local-package-repository

## Getting version information to create the divi debian package

```shell
$ divi-cli --version|cut -f 6 -d ' '|cut -f 1 -d '-'
$ uname -np|sed 's/ /_/g'
$ dver=`divi-cli --version|cut -f 6 -d ' '|cut -f 1 -d '-'`
$ dboard=`uname -np|sed 's/ /_/g'`
$ mv divi-cli.deb divi-cli-"$dver"-"$dboard".deb
```