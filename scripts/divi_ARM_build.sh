#!/bin/sh

#	Compile the programmed branch:
#		curl -s https://nk.cr/sh/ubuntu/divi/core/buildPI.sh | sh

sendMessage()	{
	echo "$(date) | ${1}"
}

ubuntuInstall()	{
	sendMessage "apt-get install -y ${1}"
	apt-get -qq install ${1} > /dev/null
	sleep 1
}

echo ""

MINSPACEINGBTOINSTALL=8
FREEDISKSPACE="$(df -h / | sed -n 2p)"
DISKSPACESIZEEND="$(echo "${#FREEDISKSPACE} - 2 " | bc)"
DISKSPACESIZESTART="$(echo "${DISKSPACESIZEEND} - 12 " | bc)"
FREEDISKSPACE=$(echo $FREEDISKSPACE | cut -c ${DISKSPACESIZESTART}-${DISKSPACESIZEEND} | cut -d' ' -f 1)
DISKSPACESIZEEND="${#FREEDISKSPACE}"
DISKSPACESIZEEND="$(echo "${DISKSPACESIZEEND} + 1 " | bc)"
DISKSPACESIZESTART="$(echo "${DISKSPACESIZEEND} - 1 " | bc)"
FREEDISKSPACETYPE=$(echo $FREEDISKSPACE | cut -c ${DISKSPACESIZESTART}-${DISKSPACESIZEEND})
FREEDISKSPACE=$(echo $FREEDISKSPACE| cut -d "${FREEDISKSPACETYPE}" -f 1)
FREEDISKSPACE=${FREEDISKSPACE%.*}
CANINSTALL="n"
if [ "${FREEDISKSPACETYPE}" = G ]; then
	if [ "${FREEDISKSPACE}" -ge $MINSPACEINGBTOINSTALL ]; then
		CANINSTALL="y"
	fi
fi
sleep 1
if [ $CANINSTALL = y ]; then
	DIVIGITHUBURL="$(curl -s https://nk.cr/sh/ubuntu/divi/core/buildDivi)"
	PIDISBUILDING="/tmp/.isBuilding"
	PIDISINSTALLED="/root/.diviBI"
	BUILDATDIVIDIR="/media/blddv/Divi/"
	
	DEBIAN_FRONTEND=noninteractive
	sendMessage "DIVI Daemon Builder Tool (for PI) by DIVI Labs, proudly supported by NK Technologies"
	
	if [ -f ${PIDISBUILDING} ]; then
		sendMessage "DIVI Builder is already running."
	else
		echo "1" > ${PIDISBUILDING}
		if [ -f ${PIDISINSTALLED} ]; then
			git clone -q ${DIVIGITHUBURL} ${BUILDATDIVIDIR}
			sendMessage "Divi cloned"
			sleep 1
			OLDDIR=${PWD}
			cd ${BUILDATDIVIDIR}/divi
			sendMessage "Preparing to Build Divi from source at ${PWD}."
			sleep 1
			./autogen.sh
			sendMessage "Divi autogen"
			sleep 1
			./configure --disable-tests --without-gui --with-unsupported-ssl
			sendMessage "Divi configure"
			sleep 1
			make
			sendMessage "Divi make"
			sleep 1
			make install
			sendMessage "Divi make install"
			sleep 1
			rm -f ${PIDISBUILDING}
			cd ${OLDDIR}
		else
			sendMessage "Installing building software."
			add-apt-repository -y "ppa:bitcoin/bitcoin" > /dev/null
			sleep 1
			apt-get -qq update > /dev/null
			apt-get -qq upgrade > /dev/null
			ubuntuInstall "git-core"
			ubuntuInstall "libssl-dev"
			ubuntuInstall "libboost-all-dev"
			ubuntuInstall "libdb4o-cil-dev"
			ubuntuInstall "libevent-dev"
			ubuntuInstall "libzmq3-dev"
			ubuntuInstall "autoconf"
			ubuntuInstall "tmux"
			ubuntuInstall "vim"
			ubuntuInstall "automake"
			ubuntuInstall "libtool"
			ubuntuInstall "libdb4.8-dev"
			ubuntuInstall "libdb4.8++-dev"
			ubuntuInstall "software-properties-common"
			apt-get -qq update > /dev/null
			apt-get -qq upgrade > /dev/null
			sleep 1
			echo "1" > ${PIDISINSTALLED}
			sendMessage "Building software installed"
			sleep 1
			rm -f ${PIDISBUILDING}
			sleep 1
			curl -s https://nk.cr/sh/ubuntu/divi/core/buildPI.sh | sh
		fi
	fi
else
	echo "You don't have enough space to install, you need at least ${MINSPACEINGBTOINSTALL} GB. ... WHATCHA doing?"
fi
echo ""
